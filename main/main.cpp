#include <iostream>

#include "loguru.hpp"

#include "MyClass.hpp"


int main(int argc, char** argv)
{
    loguru::init(argc, argv);

    LOG_F(INFO, "Starting main.");

    constexpr int n = 5;

    auto x = MyClass::fact(n);

    LOG_F(INFO, "The factorial of %i is %i", n, x);

    LOG_F(INFO, "Finished, exiting ...");
} // end main

